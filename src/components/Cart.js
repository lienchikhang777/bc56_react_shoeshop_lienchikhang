import React, { Component } from 'react'
import CartItem from './CartItem'

export default class Cart extends Component {
    convertString = (string) => {
        if (string.length >= 10) { return string.slice(0, 10) + '...' }
    }
    handleTotalAmount = (cart) => {
        return cart.reduce((accumulate, curVar) => {
            return accumulate + curVar.quantity;
        }, 0)
    }
    handleTotalPrice = cart => {
        return cart.reduce((accumulate, curVar) => {
            return accumulate + curVar.price * curVar.quantity;
        }, 0)
    }
    renderCartList = (cart) => {
        return cart.map((cartItem, index) => {
            const { handleChangeAmount, handleDeleteCartItem } = this.props;
            return (
                <CartItem
                    cartItem={cartItem}
                    handleChangeAmount={handleChangeAmount}
                    handleDeleteCartItem={handleDeleteCartItem}
                />
            )
        })
    }
    render() {
        const { data } = this.props;
        return (
            <div style={{
                padding: '16px'
            }}>
                <h3>Shopping Cart</h3>
                <div className='my-scroll' style={{
                    maxHeight: '500px',
                    height: '500px',
                    padding: '16px',
                    backgroundColor: '#ccc',
                    overflow: 'scroll',
                }}>
                    {this.renderCartList(data)}
                </div>
                <div className="total-section mt-3 p-4 bg-primary text-white">
                    <div className='d-flex justify-content-between'>
                        <h4>Total Amount:</h4>
                        <p style={{
                            fontSize: '24px'
                        }}>{this.handleTotalAmount(data)}</p>
                    </div>
                    <div className='d-flex justify-content-between mt-1'>
                        <h4>Total price:</h4>
                        <p style={{
                            fontSize: '24px'
                        }}>{this.handleTotalPrice(data)}</p>
                    </div>
                </div>
            </div>
        )
    }
}
