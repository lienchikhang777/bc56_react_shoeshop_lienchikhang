import React, { Component } from 'react'

export default class CartItem extends Component {
    convertString = (string) => {
        if (string.length >= 10) { return string.slice(0, 10) + '...' }
    }
    render() {
        const { id, name, image, price, quantity } = this.props.cartItem;
        const { handleChangeAmount, handleDeleteCartItem } = this.props;
        return (
            <div className="p-4">
                <div className="px-3 row align-items-center justify-content-between bg-white mb-2" style={{
                    borderRadius: '20px'
                }}>
                    <img className="" src={image} style={{ width: '150px' }} />
                    <div className="card-body">
                        <h4 className="card-title">{this.convertString(name)}</h4>
                        <p className="card-text">{price}</p>
                    </div>
                    <div className="action mr-4">
                        <button
                            className="btn btn-primary"
                            onClick={() => {
                                handleChangeAmount(this.props.cartItem, -1)
                            }}
                        >-</button>
                        <span className='mx-2'>{quantity}</span>
                        <button
                            className="btn btn-primary"
                            onClick={() => {
                                handleChangeAmount(this.props.cartItem, 1)
                            }}
                        >+</button>
                    </div>
                    <div className="">
                        <button
                            className='btn btn-danger'
                            onClick={() => {
                                handleDeleteCartItem(this.props.cartItem);
                            }}
                        >Delete</button>
                    </div>
                </div>
            </div>
        )
    }
}
