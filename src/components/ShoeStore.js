import React, { Component } from 'react'
import Cart from './Cart'
import ShoeList from './ShoeList'
import shoesArray from './data.js'
import ShowShoe from './ShowShoe'
export default class ShoeStore extends Component {
    state = {
        cart: [],
        shoesArray,
        viewingShoe: shoesArray[0],
    }
    handleToCart = (shoe) => {
        let cloneArray = [...this.state.cart];
        const index = cloneArray.findIndex((item) => {
            return shoe.id === item.id;
        })

        if (index == -1) cloneArray = [...this.state.cart, { ...shoe, quantity: 1 }];
        else cloneArray[index].quantity += 1;
        // const cloneArray = [...this.state.cart, { ...shoe, quantity: 1 }];
        this.setState({
            cart: cloneArray
        })
    }

    handleChangeAmount = (shoe, option) => {
        const cloneArray = [...this.state.cart];
        const indexFindingShoe = cloneArray.findIndex((item) => {
            return shoe.id === item.id;
        })
        cloneArray[indexFindingShoe].quantity += option;
        if (cloneArray[indexFindingShoe].quantity < 1) {
            cloneArray.splice(indexFindingShoe, 1)
        }
        this.setState({
            cart: cloneArray
        })
    }

    handleDeleteCartItem = (shoe) => {
        const cloneArray = [...this.state.cart];
        const index = cloneArray.findIndex((item) => {
            return shoe.id === item.id;
        })
        cloneArray.splice(index, 1);
        this.setState({
            cart: cloneArray
        })
    }

    handleView = (shoe) => {
        this.setState({
            viewingShoe: shoe
        })
    }

    render() {
        return (
            <div className="container-fuild">
                <div className="row">
                    <div className="col-6">
                        <Cart
                            data={this.state.cart}
                            handleChangeAmount={this.handleChangeAmount}
                            handleDeleteCartItem={this.handleDeleteCartItem}
                        />
                    </div>
                    <div className="col-6">
                        <ShoeList
                            data={this.state.shoesArray}
                            handleToCart={this.handleToCart}
                            handleView={this.handleView}
                        />
                    </div>
                </div>
                <div>
                    <ShowShoe
                        data={this.state.viewingShoe}
                    />
                </div>
            </div>
        )
    }
}
