import React, { Component } from 'react'
import ShoeItem from './ShoeItem'

export default class ShoeList extends Component {
    convertString = (string) => {
        if (string.length >= 10) { return string.slice(0, 10) + '...' }
    }
    renderShoeList = (list) => {
        const { handleView } = this.props;
        return list.map((item) => {
            return (
                <ShoeItem
                    item={item}
                    handleToCart={this.props.handleToCart}
                    handleView={handleView}
                />
            )
        })
    }

    render() {
        const { data } = this.props;
        return (
            <div className='row'>
                {this.renderShoeList(data)}
            </div>
        )
    }
}
