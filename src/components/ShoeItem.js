import React, { Component } from 'react'

export default class ShoeItem extends Component {
    convertString = (string) => {
        if (string.length >= 10) { return string.slice(0, 10) + '...' }
    }
    render() {
        const { id, name, price, image } = this.props.item;
        const { handleView, handleToCart } = this.props;
        return (
            <div className="col-4" key={id}>
                <div className="card b">
                    <img className="card-img-top" src={image} style={{ width: '200px' }} />
                    <div className="card-body">
                        <h4 className="card-title">{this.convertString(name)}</h4>
                        <p className="card-text">{price}</p>
                    </div>
                    <div className="card-footer">
                        <button
                            className='btn btn-success d-block mx-auto'
                            onClick={() => {
                                handleToCart(this.props.item);
                            }}
                        >Add to Cart</button>
                        <button
                            className='btn btn-primary mt-2'
                            onClick={() => {
                                handleView(this.props.item);
                            }}
                        >View</button>
                    </div>
                </div>
            </div>
        )
    }
}
