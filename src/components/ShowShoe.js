import React, { Component } from 'react'

export default class ShowShoe extends Component {
    render() {
        const { name, price, image } = this.props.data;
        return (
            <div>
                <div className="card text-center mt-4">
                    <img className="card-img-top" src={image} style={{ width: '150px', margin: '0 auto' }} />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">{price}</p>
                    </div>
                </div>
            </div>
        )
    }
}
